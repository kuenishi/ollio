exception Bad_line;;
exception Unknown;;

type r = {
  mutable pos_pos : int;
  mutable pos_neg : int;
  mutable neg_pos : int;
  mutable neg_neg : int;
};;

let new_result () =
  { pos_pos = 0; pos_neg = 0; neg_pos = 0; neg_neg = 0; };;

let update_result r y_bar sign = match y_bar, sign with
  | 1,1 ->    let i = r.pos_pos in r.pos_pos <- i + 1;
  | 1,-1 ->   let i = r.pos_neg in r.pos_neg <- i + 1;
  | -1,1 ->   let i = r.neg_pos in r.neg_pos <- i + 1;
  | -1,-1 ->  let i = r.neg_neg in r.neg_neg <- i + 1;
  | _,_ ->    raise (Invalid_argument "only 1 or -1 is accepted as result");;

let print_result r =
  let hit = r.pos_pos + r.neg_neg in
  let wrong = r.pos_neg + r.neg_pos in
  let acc = 100.0 *. (float_of_int hit) /. (float_of_int (hit + wrong)) in
  Printf.printf "Accuracy %f%% (%d/%d)\n" acc hit (wrong+hit);
  Printf.printf "(Answer, Predict): (p,p):%d (p,n):%d (n,p):%d (n,n):%d\n"
    r.pos_pos r.pos_neg r.neg_pos r.neg_neg;;

(* w x -> 1 | -1 *)
let predict w x =
  let y = Vector.prod w x in
  if y > 0.0 then 1 else -1;;

(* vector -> float -> flost *)
let pa x l =
  l /. (Vector.norm2 x);;

let pa1 c x l =
  let v = pa x l in
  if v < c then v else c;;

let pa2 c x l =
  l /. ( (Vector.norm2 x) +. 1.0 /. (2.0 *. c));;

(* "PA"|"PA1"|"PA2" -> ( vector -> float -> flost *)
let tau_getter = function
  | "PA" ->  pa;
  | "PA1" -> pa1 1.0;
  | "PA2" -> pa2 1.0;
  | _ -> raise (Invalid_argument "PA/PA1/PA2 is only accepted as learning algorithm");;

let loss y_t w x =
  let v = 1.0 -. (Vector.prod w x) *. (float_of_int y_t) in
  if v < 0.0 then 0.0 else v;;

(* int_of_string does not accept "+1" *)
let my_int_of_string s =
  if String.get s 0 == '+' then
    int_of_string (Str.string_after s 1)
  else
    int_of_string s;;

let regex_space = Str.regexp " ";;
let regex_colon = Str.regexp ":";;

let split_char sep str =
  let string_index_from i =
    try Some (String.index_from str i sep)
    with Not_found -> None
  in
  let rec aux i acc = match string_index_from i with
    | Some i' ->
      let w = String.sub str i (i' - i) in
        aux (succ i') (w::acc)
    | None ->
      let w = String.sub str i (String.length str - i) in
        List.rev (w::acc)
  in
  aux 0 []

let parse_line0 line =
  let l2l token = 
    match split_char ':' token with
      | i::v::[] -> ((my_int_of_string i), (float_of_string v));
      | _ -> raise Bad_line
  in	
  match split_char ' ' line with
    | [] -> raise Bad_line;
    | hd::tl ->
      (my_int_of_string hd), (List.map l2l tl);;

(* string -> int, [float] *)
let parse_line line =
  let l2l token =
    match Str.split regex_colon token with
      | i::v::[] -> ((my_int_of_string i), (float_of_string v));
      | _ -> raise Bad_line
  in
  match Str.split regex_space line with
    | [] -> raise Bad_line;
    | hd::tl ->
      (my_int_of_string hd), (List.map l2l tl);;

type line_t = Line of string | EOF;;

let get_line input_in =
  try
    Line(input_line input_in)
  with
      End_of_file -> EOF;;


(* "PA"|"PA1"|"PA2" -> string -> vector *)
let train t file =
  let input_in = open_in file in
  let f_tau = tau_getter t in
  let rec train_ w =
    match get_line input_in with
      | Line(line) ->
	let sign,x = parse_line0 line in
	let tau = f_tau x (loss sign w x) in
	train_ (Vector.w_plus_yx w (tau *. float_of_int sign) x);
      | EOF ->
	close_in input_in;
	w
  in
  train_ (Vector.newv());;


(* "PA"|"PA1"|"PA2" -> vector -> file -> Oll.result *)
let test ww t file = 
  let input_in = open_in file in
  let f_tau = tau_getter t in
  let r = new_result () in
  let rec test_ w =
    match get_line input_in with
      | Line(line) ->
	let sign,x = parse_line line in
	let y_bar = predict w x in
	let tau = f_tau x (loss sign w x) in
	update_result r y_bar sign;
(*      Printf.printf "(y_bar sign)=(%d, %d) (%f)\n" y_bar sign tau; *)
(*      Vector.print_vector x;
      Vector.print_vector w;
      Vector.print_vector (Vector.add w (Vector.s_prod (tau *. float_of_int sign) x)); *)
	test_ (Vector.w_plus_yx w (tau *. float_of_int sign) x);
(*	test_ (Vector.add w (Vector.s_prod (tau *. float_of_int sign) x)); *)
      | EOF ->
	close_in input_in;
	r
  in
  test_ ww;;
