
let get_input_filename default = 
  if Array.length Sys.argv > 1 then 
    Array.get Sys.argv 1
  else default;;

let usage() =
  print_endline "usage: ./mlml <algorithm> <training data file> <test data file>";;

let a() =
  if Array.length Sys.argv != 4 then
    usage()
  else begin
    let algorithm = Array.get Sys.argv 1 in
    let trainfile = Array.get Sys.argv 2 in
    let testfile  = Array.get Sys.argv 3 in
    Printf.printf "algorithm\t%s\n" algorithm;
    Printf.printf "traindata\t%s\n" trainfile;
    Printf.printf "testdata\t%s\n" testfile;
    let w = Oll.train algorithm trainfile in
    print_endline "train done";
    let r = Oll.test w algorithm testfile in
    Oll.print_result r;
  end;;

let _ =
  a();;
(*  let w = Vector.newv() in
  let x = [(1,234.0)] in
  let y = Vector.add w x in
  let z = Vector.add x [(23, -0.342); (934,-23.4)] in
  Vector.print_vector w;
  Vector.print_vector x;
  Vector.print_vector y;
  Vector.print_vector z;
  Vector.print_vector (Vector.add z z);
  Vector.print_vector (Vector.add z [(2, -0.342)]);; *)
(*
  INPUT: aggressiveness parameter C > 0
  INITIALIZE: w1 = (0,...,0)
  For	t = 1,2,...
    • receive instance: xt ∈ Rn
    • predict: yˆt = sign(wt · xt )
    • receive correct label: yt ∈ {−1, +1}
    • suffer loss: lt = max{0 , 1−yt(wt ·xt)}
  • update:
    1. set:
       τt = lt / |x_t|^2	(PA) ∥xt∥
       τt =min C, lt/∥xt∥2	(PA-I)
       τ= lt / (|xt|^2 + 1/2C)  (PA-II)
    2.update: wt+1=wt+τtytxt
*)
